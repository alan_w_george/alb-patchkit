# PatchKit
The purpose of the project is to explore implementing current DCS functionality as a Kotlin Miltiplatform Module ([KMM](https://kotlinlang.org/lp/mobile/)).  This allows a single implementation for some types of business logic across iOS and Android.

Project currently consist of an Android and iOS Demo applications and a one KMM share module called patchkitshared.

The NFC and BLE low level stuff would be platform specific parts (expect/actual) to the KMM, but the rest could be common code. 

Currently, it's just a POC of sharing some minimal code between platforms.  There is no NFC of BLU code as yet.

PatchInfo - Decodes a byte array in to a usable PatchInfo object including CRC validation.

![](./doc/images/Kmm.png)
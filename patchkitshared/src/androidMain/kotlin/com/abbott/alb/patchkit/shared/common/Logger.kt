package com.abbott.alb.patchkit.shared.common

import timber.log.Timber

actual class Logger actual constructor(val tag: String) {
    actual fun d(message: String) {
        Timber.d("[$tag] $message")
    }
}
package com.abbott.alb.patchkit.shared

class Greeting {
    fun greeting(): String {
        return "Hello from PatchKit, ${Platform().platform}!"
    }
}
package com.abbott.alb.patchkit.shared.common

expect class Logger(tag: String) {
    fun d(message: String)
}
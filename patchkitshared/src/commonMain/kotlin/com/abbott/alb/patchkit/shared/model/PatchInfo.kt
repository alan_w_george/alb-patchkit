package com.abbott.alb.patchkit.shared.model

import com.abbott.alb.patchkit.shared.common.Crc16
import com.abbott.alb.patchkit.shared.common.Logger
import com.abbott.alb.patchkit.shared.common.toUnsignedLittleEndian


data class PatchInfo(
    val securityVersion: Int,
    val generation: Int,
    val localization: Int,
    val wearDuration: Int,
    val warmupDuration: Int,
    val DMX: List<Byte>,
    val fwVersion: List<Byte>,
    val productType: Int,
    val state: Int,
    val crc: Int,
    val calculatedCrc : Int,
    val serialNumber: String = DMX.joinToString(separator = "") {
        Char(it.toInt()).toString()
    }
) {
    internal companion object {
        const val DATA_START_BYTE = 0xa5.toByte()
        const val RESPONSE_SIZE = 27   // Includes ISO error flag
        val INDEX_LOCALIZATION = { offset: Int ->  offset + 2 }
        val INDEX_GENERATION = { offset: Int ->  offset + 4 }
        val INDEX_WEAR_DURATION = { offset: Int ->  offset + 6 to offset + 8 }
        val INDEX_SECURITY_VERSION = { offset: Int ->  offset to offset + 2 }
        val INDEX_PRODUCT_TYPE = { offset: Int ->  offset + 12 }
        val INDEX_WARMUP_DURATION = { offset: Int ->  offset + 13 }
        val INDEX_PATCH_STATE = { offset: Int ->  offset + 14 }
        val INDEX_FW_VERSION = { offset: Int ->  offset + 8 to offset + 12 }
        val INDEX_DMX = { offset: Int ->  offset + 15 to offset + 24 }
        val INDEX_CRC = {offset: Int -> offset + 24 to offset + 26 }
        const val MINUTES_PER_NFC_WARMUP_TIME_COUNT = 5
    }
}

private val logger = Logger("PatchInfo")

@Throws(Exception::class)
fun ByteArray.buildPatchInfo(): PatchInfo = buildPatchInfoResult().getOrThrow()

fun ByteArray.buildPatchInfoResult(): Result<PatchInfo> = runCatching {
    val offset = indexOf(PatchInfo.DATA_START_BYTE) + 2 // 1 to skip start marker and 1 for some flags byte

    require(size - offset != PatchInfo.RESPONSE_SIZE) {
        "patch info source not valid: size - offset != ${PatchInfo.RESPONSE_SIZE}"
    }
    val wearDuration = PatchInfo.INDEX_WEAR_DURATION(offset).let { (start, end) ->
        copyOfRange(start, end).toUnsignedLittleEndian()
    }
    val securityVersion = PatchInfo.INDEX_SECURITY_VERSION(offset).let { (start, end) ->
        copyOfRange(start, end).toUnsignedLittleEndian()
    }
    val warmupDuration = PatchInfo.INDEX_WARMUP_DURATION(offset).let { index ->
        get(index = index).toInt() * PatchInfo.MINUTES_PER_NFC_WARMUP_TIME_COUNT
    }
    val state = PatchInfo.INDEX_PATCH_STATE(offset).let { index ->
        get(index = index).toInt()
    }
    val localization = PatchInfo.INDEX_LOCALIZATION(offset).let { index ->
        get(index = index).toInt()
    }
    val generation = PatchInfo.INDEX_GENERATION(offset).let { index ->
        get(index = index).toInt()
    }
    val productType = PatchInfo.INDEX_PRODUCT_TYPE(offset).let { index ->
        get(index = index).toInt()
    }
    val fwVersion = PatchInfo.INDEX_FW_VERSION(offset).let { (start, end) ->
        copyOfRange(start, end).toList()
    }
    val DMX = PatchInfo.INDEX_DMX(offset).let { (start, end) ->
        copyOfRange(start, end).toList()
    }
    val crcPair = PatchInfo.INDEX_CRC(offset).let { (start, end)  ->
        val crcCalculated = Crc16.calculate(this, offset, start)
        val crc = copyOfRange(start, end).toUnsignedLittleEndian()

        logger.d("crc:$crc crcCalculated:$crcCalculated")

        crc to crcCalculated
    }

    PatchInfo(
        wearDuration = wearDuration,
        warmupDuration = warmupDuration,
        state = state,
        localization = localization,
        generation = generation,
        productType = productType,
        securityVersion = securityVersion,
        fwVersion = fwVersion,
        DMX = DMX,
        crc = crcPair.first,
        calculatedCrc = crcPair.second
    )
}
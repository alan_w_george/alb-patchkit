package com.abbott.alb.patchkit.shared

expect class Platform() {
    val platform: String
}
package com.abbott.alb.patchkit.shared.common

internal fun ByteArray.toUnsignedLittleEndian() =
    map(Byte::toUByte).foldIndexed(0) { index, accum, b ->
        val shifted = b.toInt() shl (8 * index)
        accum or shifted
    }

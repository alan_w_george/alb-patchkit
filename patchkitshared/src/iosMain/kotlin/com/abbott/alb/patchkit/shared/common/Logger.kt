package com.abbott.alb.patchkit.shared.common

actual class Logger actual constructor(val tag: String) {
    actual fun d(message: String) {
        println("[$tag] $message")
    }
}
import SwiftUI

@main
struct iOSApp: App {
    let rawPatchInfo: [UInt8] = [0, 165, 0, 1, 0, 0, 0, 0, 0, 80, 1, 253, 0, 1, 0, 4, 12, 2, 48, 54, 56, 90, 72, 65, 68, 57, 70, 86, 175]
	var body: some Scene {
		WindowGroup {
            ContentView(viewModel: .init(rawPatchInfo: rawPatchInfo))
		}
	}
}

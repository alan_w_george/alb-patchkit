import SwiftUI
import patchkitshared

struct ContentView: View {
    @ObservedObject private(set) var viewModel: ViewModel
    
    var body: some View {
        VStack(alignment: .center, spacing: 10.0) {
            Text(viewModel.greeting).padding()
            Text("Raw Patch Info").bold()
            Text(viewModel.rawPatchInfoAsString).padding()
        }
        
        getView()
    }
    
    private func getView() -> AnyView {
        switch viewModel.patchInfoStatus {
        case .loading:
            return AnyView(Text("Loading..."))
        case .error(let message):
            return AnyView(Text("error decoding PatchInfo: \(message)"))
        case .result(let patchInfo):
            return AnyView(
                VStack(alignment: .center, spacing: 10.0) {
                    Group { // can only have 10 Views using Group as a workaround
                        Text("Decoded PatchInfo").bold()
                        Text("serialNumber: \(patchInfo.serialNumber)")
                        Text("securityVersion: \(patchInfo.securityVersion)")
                        Text("localization: \(patchInfo.localization)")
                    }
                    Group {
                        Text("generation: \(patchInfo.generation)")
                        Text("wearDuration: \(patchInfo.wearDuration)")
                        Text("warmupDuration: \(patchInfo.warmupDuration)")
                        Text("crc: \(patchInfo.crc)")
                        Text("calculatedCrc: \(patchInfo.calculatedCrc)")
                        Text("fwVersion: [\(patchInfo.fwVersion.toString())]")
                        Text("DMX: [\(patchInfo.DMX.toString())]")
                    }
                }
            )
        }
    }
}

enum PatchInfoResults {
    case loading
    case result(PatchInfo)
    case error(String)
}

extension ContentView {
    class ViewModel : ObservableObject {
        let rawPatchInfo: [UInt8]
        let rawPatchInfoAsString: String
        let greeting = Greeting().greeting()
        @Published var patchInfoStatus = PatchInfoResults.loading
        
        init(rawPatchInfo: [UInt8]) {
            self.rawPatchInfo = rawPatchInfo
            self.rawPatchInfoAsString = rawPatchInfo.toHexString()
            self.decodePatchInfo()
        }
        
        func decodePatchInfo() {
            let patchInfo = Result { try rawPatchInfo.toKotlinByteArray().buildPatchInfo() }

            switch patchInfo {
            case .success(let success):
                patchInfoStatus = PatchInfoResults.result(success)
            case .failure(let failure):
                patchInfoStatus = PatchInfoResults.error(failure.localizedDescription)
            }
        }
    }
}

extension Array where Element == UInt8 {
    func toKotlinByteArray() -> KotlinByteArray {
        let intArray = map {
            Int8(bitPattern: $0)
        }
        let kotlinByteArray: KotlinByteArray = KotlinByteArray.init(size: Int32(count))
        for (index, element) in intArray.enumerated() {
            kotlinByteArray.set(index: Int32(index), value: element)
        }
        
        return kotlinByteArray
    }
    func toHexString() -> String {
        map {
            String(format: "%02x", $0)
        }.joined()
    }
}

extension Array where Element == KotlinByte {
    func toString() -> String {
        return map {
            String(format: "%02x", $0.uint8Value)
        }.joined(separator: ", ")
    }
    func toUByteArray() -> [UInt8] {
        return map {
            $0.uint8Value
        }
    }
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
        let rawPatchInfo: [UInt8] = [0, 165, 0, 1, 0, 0, 0, 0, 0, 80, 1, 253, 0, 1, 0, 4, 12, 2, 48, 54, 56, 90, 72, 65, 68, 57, 70, 86, 175]
        ContentView(viewModel: .init(rawPatchInfo: rawPatchInfo))
	}
}

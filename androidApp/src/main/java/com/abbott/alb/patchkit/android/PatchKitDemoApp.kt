package com.abbott.alb.patchkit.android

import androidx.compose.foundation.layout.*
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import arrow.core.Either
import com.abbott.alb.patchkit.android.ui.theme.PatchKitDemoTheme
import com.abbott.alb.patchkit.android.ui.viewmodel.PatchInfoViewModel
import com.abbott.alb.patchkit.android.ui.viewmodel.toHex
import com.abbott.alb.patchkit.shared.Greeting
import com.abbott.alb.patchkit.shared.model.PatchInfo

@Composable
fun PatchKitDemoApp() {
    PatchKitDemoTheme {
        Scaffold {
            val viewModel = viewModel<PatchInfoViewModel>()
            val rawDataHex = viewModel.patchInfoRaw.map {
                it.toHex
            }.getOrElse { "invalid raw data" }

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top
            ) {
                Text(text = Greeting().greeting())
                Spacer(modifier = Modifier.height(32.dp))
                Text(text = "Raw PatchInfo Hex Data", fontWeight = FontWeight.Bold)
                Text(text = rawDataHex)
                Spacer(modifier = Modifier.height(32.dp))
                Text(text = "Decoded PatchInfo", fontWeight = FontWeight.Bold)
                viewModel.patchInfo.fold(
                    { PatchInfo(patchInfo = it) },
                    { Text(text = "error getting PatchInfo: ${it.localizedMessage}") }
                )
            }
        }
    }
}

@Composable
fun PatchInfo(patchInfo: PatchInfo) {
    Column(modifier = Modifier.fillMaxWidth()) {
        PatchInfoRow(name = "serialNumber", value = patchInfo.serialNumber)
        PatchInfoRow(name = "securityVersion", value = patchInfo.securityVersion.toString())
        PatchInfoRow(name = "localization", value = patchInfo.localization.toString())
        PatchInfoRow(name = "generation", value = patchInfo.generation.toString())
        PatchInfoRow(name = "wearDuration", value = patchInfo.wearDuration.toString())
        PatchInfoRow(name = "warmupDuration", value = patchInfo.warmupDuration.toString())
        PatchInfoRow(name = "fwVersion", value = patchInfo.fwVersion.map { it.toUByte().toString(16)  }.toString())
        PatchInfoRow(name = "DMX", value = patchInfo.DMX.map { it.toUByte().toString(16)  }.toString())
        PatchInfoRow(name = "CRC", value = patchInfo.crc.toString())
        PatchInfoRow(name = "Calculated CRC", value = patchInfo.calculatedCrc.toString())
    }
}

@Composable
fun PatchInfoRow(name: String, value: String) {
    Row(modifier = Modifier.fillMaxWidth()) {
        Text(
            modifier = Modifier.weight(1f),
            text = name,
            fontWeight = FontWeight.Bold
        )
        Text(modifier = Modifier.weight(1.5f), text = value)
    }
}

@Preview(showBackground = true)
@Composable
fun PatchInfoPreview() {
    val patchInfo = PatchInfo(
        securityVersion = 1,
        generation = 0,
        localization = 0,
        wearDuration = 336,
        warmupDuration = 60,
        productType = 4,
        state = 2,
        fwVersion = listOf(-3, 0, 1, 0).map(Int::toByte),
        DMX = listOf(48, 54, 56, 90, 72, 65, 68, 57, 70).map(Int::toByte),
        crc = 12345,
        calculatedCrc = 12345
    )

    PatchInfo(patchInfo = patchInfo)
}

@Preview(showBackground = true)
@Composable
fun PatchInfoRowPreview(
    @PreviewParameter(PatchInfoRowPreviewDataProvider::class) data: Pair<String, String>
) {
    PatchInfoRow(name = data.first, value = data.second)
}

class PatchInfoRowPreviewDataProvider: PreviewParameterProvider<Pair<String, String>> {
    override val values: Sequence<Pair<String, String>>
        get() = sequenceOf(
            "Name" to "Value",
            "wearDuration" to "336",
            "serialNumber" to "068ZHAD9F"
        )
}
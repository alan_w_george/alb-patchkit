package com.abbott.alb.patchkit.android.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.abbott.alb.patchkit.shared.model.buildPatchInfoResult

class PatchInfoViewModel: ViewModel() {
    val patchInfoRaw = "00a5000100000000005001fd000100040c023036385a484144394656af".toByteArray()
//    val patchInfoRaw = listOf(0, 165, 0, 1, 0, 0, 0, 0, 0, 80, 1, 253, 0, 1, 0, 4, 12, 2, 48, 54, 56, 90, 72, 65, 68, 57, 70, 86, 17).map { it.toUByte() }

    val patchInfo = patchInfoRaw.flatMap { it.buildPatchInfoResult() }
}

fun String.toByteArray(): Result<ByteArray> = runCatching {
    check(length % 2 == 0) { "must be an even length string" }

    chunked(2)
        .map { it.toInt(16) }
        .map(Int::toByte)
        .toByteArray()
}

val ByteArray.toHex
    get() = joinToString(separator = "") {
        "%02x".format(it)
    }

fun <T, R> Result<T>.flatMap(block: (T) -> Result<R>) =
    fold({ block(it) }, { Result.failure(it) })

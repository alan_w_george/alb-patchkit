import org.jetbrains.kotlin.config.JvmAnalysisFlags.useIR

plugins {
    id("com.android.application")
    kotlin("android")
}

object Versions {
    const val compose = "1.0.5"
}

android {
    compileSdk = 31
    defaultConfig {
        applicationId = "com.abbott.alb.patchkit.android"
        minSdk = 21
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.1.0-rc02"
    }
}

dependencies {
    implementation(project(":patchkitshared"))
    implementation("com.google.android.material:material:1.4.0")

    implementation("androidx.compose.ui:ui:${Versions.compose}")
    // Tooling support (Previews, etc.)
    implementation("androidx.compose.ui:ui-tooling:${Versions.compose}")
    // Foundation (Border, Background, Box, Image, Scroll, shapes, animations, etc.)
    implementation("androidx.compose.foundation:foundation:${Versions.compose}")
    // Material Design
    implementation("androidx.compose.material:material:${Versions.compose}")
    // Material design icons
    implementation("androidx.compose.material:material-icons-core:${Versions.compose}")
    implementation("androidx.compose.material:material-icons-extended:${Versions.compose}")
    // Integration with observables
    implementation("androidx.compose.runtime:runtime-livedata:${Versions.compose}")
    implementation("androidx.compose.runtime:runtime-rxjava2:${Versions.compose}")

    implementation("androidx.appcompat:appcompat:1.4.0")

    implementation("com.google.accompanist:accompanist-navigation-animation:0.21.0-beta")

    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.4.0")
    implementation("androidx.activity:activity-compose:1.4.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.4.0")
    implementation("com.jakewharton.timber:timber:5.0.1")

    implementation("android.arch.lifecycle:livedata:1.1.1")
    implementation("androidx.fragment:fragment-ktx:1.4.0")
    implementation("io.arrow-kt:arrow-core:1.0.1")
    implementation("com.google.accompanist:accompanist-permissions:0.21.4-beta")
    implementation("com.jakewharton.timber:timber:5.0.1")
}
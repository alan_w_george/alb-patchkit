pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "ALB_Patch_Kit"
include(":androidApp")
include(":patchkitshared")
